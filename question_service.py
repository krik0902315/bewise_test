from services.questions_service.app import factory
import uvicorn
import logging

logging.basicConfig(level=logging.INFO)


app = factory()

if __name__ == "__main__":
    uvicorn.run(
        app="question_service:app",
        host="0.0.0.0",
        port=8010,
        reload=True
    )