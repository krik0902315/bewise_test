import asyncio
from asyncpg import Pool
from aiohttp import ClientSession
from db import (
    questions as questions_db
)
from fastapi import Request
from misc import question_fetcher


async def get_fetch_queue(request: Request) -> asyncio.Queue:
    return request.app.state.fetch_task


class FetchState:
    def __init__(self, db_pool: Pool, http_client: ClientSession):
        self.db_pool: Pool = db_pool

        self.http_client: ClientSession = http_client
        self.queue = asyncio.Queue()
        self.task: asyncio.Task = asyncio.create_task(handler(self))


async def init(db_pool: Pool, http_client: ClientSession) -> FetchState:
    return FetchState(db_pool, http_client)


async def close(fetch_process: FetchState) -> None:
    fetch_process.task.cancel()


async def handle(questions_count: int, instance: FetchState) -> None:
    await instance.queue.put(questions_count)


async def handler(state: FetchState):
    db_pool = state.db_pool
    fetch_queue = state.queue
    client = state.http_client
    while True:
        try:
            async with db_pool.acquire() as conn:
                count = await fetch_queue.get()
                if id is None:
                    continue
                questions_response = []
                while len(questions_response) != count:
                    resp = await question_fetcher.fetch_random_questions(
                        session=client,
                        questions_count=count
                    )
                    if resp:
                        for i in resp:
                            if not await questions_db.check_question_exists(
                                    conn=conn,
                                    question_id=i.id
                            ):
                                item = await questions_db.add_question(
                                    conn=conn,
                                    model=i
                                )

                                if item:
                                    questions_response.append(item)

        except (asyncio.CancelledError, GeneratorExit):
            raise

        fetch_queue.task_done()
        count = None
