from fastapi import (
    FastAPI,
    APIRouter
)
from . import (
    questions
)


def register_routes(app: FastAPI) -> FastAPI:
    router = APIRouter(prefix="/api/v1")

    router.include_router(
        questions.router
    )

    app.include_router(
        router
    )
    return app
