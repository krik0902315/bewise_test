import asyncio
import logging
from fastapi import (
    APIRouter,
    Depends
)
from models import questions
from services.questions_service.fetch_process import (
    get_fetch_queue,
    handle, FetchState
)
from db import questions as questions_db
from asyncpg import Connection
from misc.depends.db import get_db

logger = logging.getLogger(__name__)

router = APIRouter(prefix="/questions")


@router.post("/", response_model=questions.QuestionSuccessResponse)
async def fetch_questions(
        fetch_model: questions.FetchQuestions,
        fetch_queue: FetchState = Depends(get_fetch_queue),
        conn: Connection = Depends(get_db)
):
    last_question = await questions_db.get_last_question(conn)
    await handle(fetch_model.questions_num, fetch_queue)
    return questions.QuestionSuccessResponse(
        data=last_question
    )
