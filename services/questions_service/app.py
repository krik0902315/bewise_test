import logging
import os
from fastapi import FastAPI
from .routes import register_routes
from .state import State
from services.questions_service import fetch_process
from misc import db, config, question_fetcher

logger = logging.getLogger(__name__)

CONFIG_ENV = "SRVC_CONFIG"


def factory() -> FastAPI:
    conf = config.read_config(os.environ.get(CONFIG_ENV))

    state = State(conf=conf)

    app = FastAPI()
    app.state = state
    state.app = app
    register_routes(app)
    register_startup(app)
    #register_shutdown(app)
    return app


def register_startup(app: FastAPI):
    @app.on_event('startup')
    async def handler_startup():
        try:

            await startup(app)
        except:
            logger.exception("Startup crashed")


def register_shutdown(app: FastAPI):
    @app.on_event("shutdown")
    async def handler_shutdown():
        try:
            await shutdown(app)
        except:
            logger.exception("Shutdown crashed")


async def startup(app: FastAPI):
    app.state.db_pool = await db.init(app.state.config['db'])
    app.state.http_client = await question_fetcher.init()
    app.state.fetch_task = await fetch_process.init(
        app.state.db_pool,
        app.state.http_client
    )
    return app


async def shutdown(app: FastAPI):
    if pool := app.state.db_pool:
        await db.close(pool)
    if client := app.state.http_client:

        await client.close()
    if fetch_state := app.state.fetch_task:
        await fetch_process.close(fetch_state)
