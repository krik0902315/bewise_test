from asyncio import Task

from asyncpg import Pool
from aiohttp import ClientSession

from services.questions_service.fetch_process import FetchState


class State:
    def __init__(self, conf: dict):
        self.config: dict = conf
        self.db_pool: Pool = None
        self.http_client: ClientSession = None
        self.fetch_task: FetchState = None


