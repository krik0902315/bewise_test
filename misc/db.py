from typing import TypeVar, Type, Optional
import asyncpg

import logging

from pydantic.main import BaseModel

logger = logging.getLogger(__name__)

ModelCls = TypeVar('ModelCls', bound=BaseModel)


async def init(config: dict) -> asyncpg.Pool:
    dsn = config.get("dsn")
    if not dsn:
        raise RuntimeError("Db config not defined")
    return await asyncpg.create_pool(dsn)


async def close(db: asyncpg.Pool | asyncpg.Connection):
    await db.close()


def record_to_model_list(
        model_cls: Type[ModelCls],
        records: list[asyncpg.Record]
) -> list[ModelCls]:
    if records:
        models = []
        for i in records:
            record_model = record_to_model(model_cls, i)
            if record_model is not None:
                models.append(record_model)
        return models
    return []


def record_to_model(model_cls: Type[ModelCls], record: Optional[asyncpg.Record]) -> Optional[ModelCls]:
    if record:
        return model_cls.parse_obj(record)
    return None
