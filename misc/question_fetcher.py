import logging

from aiohttp import ClientSession
from models.questions import Question

BASE_URL = "https://jservice.io"

logger = logging.getLogger(__name__)


async def init():
    session = ClientSession(
        base_url=BASE_URL
    )
    return session


async def fetch_random_questions(
        session: ClientSession,
        questions_count: int
) -> list[Question] | None:
    resp = await session.get(f"/api/random?count={questions_count}")
    if resp.status != 200:
        return None
    return json_to_question_model(await resp.json())


def json_to_question_model(data: list) -> list[Question]:
    return [Question.parse_obj(i) for i in data]
