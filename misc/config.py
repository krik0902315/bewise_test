import json
import os


def read_config(path: str) -> dict:
    if not os.path.exists(path):
        return None
    with open(path, 'r') as fd:
        return json.load(fd)