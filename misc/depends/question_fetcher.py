import logging
from fastapi import Request
from aiohttp import ClientSession

logger = logging.getLogger(__name__)


async def get_fetcher(request: Request) -> ClientSession:
    try:
        client = request.app.state.http_client
    except AttributeError:
        raise RuntimeError("Application state has no http client")
    else:
        return client
