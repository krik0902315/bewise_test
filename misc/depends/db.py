import logging
from fastapi import Request
from asyncpg import Connection

logger = logging.getLogger(__name__)


async def get_db(request: Request) -> Connection:
    try:
        pool = request.app.state.db_pool
    except AttributeError:
        raise RuntimeError("Application state has no db pool")
    else:
        async with pool.acquire() as conn:
            yield conn
