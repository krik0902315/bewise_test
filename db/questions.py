from models import questions
from asyncpg import Connection
from misc.db import (
    record_to_model_list,
    record_to_model
)

TABLE = "questions"


async def add_question(
        conn: Connection,
        model: questions.Question
) -> questions.Question | None:
    new_question_dict = model.dict()

    return record_to_model(
        questions.Question,
        await conn.fetchrow(
            f"""
            INSERT INTO {TABLE}
            ({",".join([k for k in new_question_dict.keys()])})
            VALUES ({",".join([f"${i + 1}" for i in range(len(new_question_dict.keys()))])})
            RETURNING *
            """,
            *new_question_dict.values()
        )
    )


async def get_last_question(
        conn: Connection
) -> questions.Question | None:
    return record_to_model(
        questions.Question,
        await conn.fetchrow(
            f"""
            SELECT * FROM {TABLE}
            WHERE pk = (SELECT max(pk) FROM {TABLE})
            """
        )
    )


async def check_question_exists(
        conn: Connection,
        question_id: int
) -> bool:
    result = await conn.fetchrow(
        f"""
        SELECT count(*) FROM {TABLE} WHERE id = $1
        """,
        question_id
    )
    return bool(result['count'])
