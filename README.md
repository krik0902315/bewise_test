
## Сборка и запуск:
- docker-compose build
- docker-compose up

## или
- docker-compose up --build


<p>Dockerfile, файл конфигурации и миграции лежат в папке ./etc</p>

## port-forwarding:
- db : 5433
- question service : 8010

## пример запроса
- curl -X 'POST' \
  'http://0.0.0.0:8010/api/v1/questions/' \
  -H 'accept: application/json' \
  -H 'Content-Type: application/json' \
  -d '{
  "questions_num": 10
}'

