-- migrate:up

CREATE TABLE questions (
    pk BIGSERIAL PRIMARY KEY,
    id BIGINT NOT NULL,
    answer VARCHAR(255) DEFAULT '',
    question TEXT DEFAULT '',
    created_at TIMESTAMP WITH TIME ZONE
);





-- migrate:down


DROP TABLE questions;