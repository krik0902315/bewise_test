from datetime import datetime
from typing import Optional

from pydantic import (
    BaseModel,
    validator
)


class FetchQuestions(BaseModel):
    questions_num: int

    @validator("questions_num")
    def validate_count(cls, value):
        if value <= 0:
            raise ValueError("questions_num must be more than 0")
        return value


class Question(BaseModel):
    id: int
    answer: str
    question: str
    created_at: datetime


class QuestionsListData(BaseModel):
    page: int
    limit: int
    total: int
    items: list[Question] = []


class QuestionsListSuccessResponse(BaseModel):
    success: bool = True
    data: QuestionsListData


class QuestionSuccessResponse(QuestionsListSuccessResponse):
    data: Optional[Question]
